
import { createStackNavigator } from '@react-navigation/stack';
import UserDetailContainer from "./userDetail/UserDetailContainer";
import React from 'react'
import UserListContainer from './user/UserListContainer';
import { NavigationContainer } from '@react-navigation/native';
import AddUser from './userDetail/AddUser';
import CameraApp from './CameraApp';
  const Stack = createStackNavigator();
  
  class AppNavigator extends React.Component {
      render() {
    return (
        <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{ gestureEnabled: false }}
      >
        <Stack.Screen
          name="Home"
          component={UserListContainer}
          options={{ title: 'Kredit Pensiun',  }}
        />
        <Stack.Screen
          name="Profile"
          component={UserDetailContainer}
          options={{title: "Detail"}}
        />
        <Stack.Screen
          name="AddNew"
          component={AddUser}
          options={{title: "Add New"}}
          initialParams={{ user: 'me' }}
        />

          <Stack.Screen
          name="Camera"
          component={CameraApp}
          options={{title: "Camera"}}
          initialParams={{ user: 'me' }}
        />
      </Stack.Navigator>
      </NavigationContainer>
    );
    }
  }


  export default AppNavigator
  