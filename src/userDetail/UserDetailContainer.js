import React,{ Component } from "react";
import {Picker} from '@react-native-community/picker';
import { View,Text,Image,StyleSheet, Button, DatePickerAndroid, Alert } from "react-native";
import { getUsersById, deleteDataUserById, updateDataUser } from "../user/UserService";
import { TextInput, ScrollView, TouchableOpacity, TouchableHighlight } from "react-native-gesture-handler";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { DatePicker } from 'native-base'
import { handleUpdateUserNik } from "../user/UserAction";


class UserDetailContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            valueById: [],
            info: {
                    nik:"",
                    name:"",
                    address:"",
                    birth_date:"",
                    telp:"",
                    status_kepegawaian:"Aktif",
                    gaji:"",
                    bank:"",
                    respon:""
            },
            radio_props : [
              {label: 'Aktif Bekerja        ', value: 0 },
              {label: 'Pensiunan', value: 1 }
            ]
        }
        
    }
    
    componentDidMount=async()=>{
        const {passingValue} = this.props.route.params
        let data = await getUsersById(passingValue);
        const {refresh} = this.props.route.params
 
        this.setState({valueById: data})
    }
    handleDeleteUser=async(id)=>{
        let payload = await deleteDataUserById(id)
    }

    handleNikUpdate=(text)=>{
        const {passingAll} = this.props.route.params
        passingAll.dispatch({...handleUpdateUserNik, nik: text})
    }

    handleEditUser=async(data)=>{
        Alert.alert(
            'Apakah anda yakin telah mengedit dengan benar ?',
            'Setiap data yang anda edit akan kami simpan dikantor pusat. Perhatikan kembali data-data yang anda input ?',
            
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
              },
              { text: 'OK', onPress: () => updateDataUser(data) }
            ],
            { cancelable: false }
        );
    }
    render() {        
      

        return (
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                  <TouchableHighlight style={styles.camera} onPress={()=>this.props.navigation.navigate("Camera")}>
                  <Image source={{uri: 'https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg'}}
                style={styles.img}
                />
                  </TouchableHighlight>
                <Text style={styles.titleName}>NIK</Text>
                <TextInput style={styles.name}
                           keyboardType="number-pad"
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  nik: text
                                }
                              };
                            })
                          }
                           value={String(this.state.valueById.nik)}
                />
                <Text style={styles.titleName}>Nama</Text>
                <TextInput style={styles.name}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  name: text
                                }
                              };
                            })
                          }
                           value={this.state.valueById.name}
                />
                <Text style={styles.titleName}>Tanggal Lahir</Text>
                <Text>
                {"\n"}
                {"\n"}
                </Text>
                
                <DatePicker
                    minimumDate={new Date(2018, 1, 1)}
                    maximumDate={new Date(2018, 12, 31)}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    defaultDate={this.state.valueById.birth_date}
                    androidMode={"default"}
                    placeHolderText={this.state.valueById.birth_date}
                    textStyle={{ color: "green" }}
                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                    onDateChange={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  birth_date: text
                                }
                              };
                            })
                          }
                    disabled={false}
                 />
                <Text style={styles.titleName}>Alamat</Text>
                <TextInput style={styles.name}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  address: text
                                }
                              };
                            })
                          }
                           value={this.state.valueById.address}
                />
                <Text style={styles.titleName}>No Telp</Text>
                <TextInput style={styles.name}
                           keyboardType="phone-pad"
                           numericvalue
                           defaultValue={this.state.valueById.telp}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  telp: text
                                }
                              };
                            })
                          }
                           value={String(this.state.valueById.telp)}
                />
                <Text style={styles.titleName}>Status Kepegawaian</Text>
                <Text>
                {"\n"}
                </Text>
                <RadioForm
                    radio_props={this.state.radio_props}
                    initial={0}
                    style={styles.radio}
                    onPress={(value) => {this.setState({radio_props:value})}}
                    formHorizontal={true}
                 />
                 <Text>
                {"\n"}
                </Text>
                 <Text style={styles.titleName}>Gaji Aktif/Pensiun</Text>
                <TextInput style={styles.name}
                           keyboardType="decimal-pad"
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  gaji: text
                                }
                              };
                            })
                          }
                           value={String(this.state.valueById.gaji)}
                />

                <Text style={styles.titleName}>Pembayaran Gaji Dengan Bank</Text>
                <Picker
                 selectedValue={this.state.valueById.bank} 
                 style={styles.picker}
                 defaultValue={this.state.valueById.bank} 
                 onValueChange={text =>
                    this.setState(state => {
                      return {
                        ...state,
                        valueById: {
                          ...state.valueById,
                          bank: text
                        }
                      };
                    })
                  }
                >
                    <Picker.Item label="BCA" value="BCA"/>
                    <Picker.Item label="BNI" value="BNI"/>
                    <Picker.Item label="Mandiri" value="Mandiri"/>

                </Picker>

                <Text style={styles.titleName}>Respon</Text>
                <TextInput style={styles.name}
                            label="Respon"
                           underlineColorAndroid='rgba(0,0,0,0)'
                           onChangeText={text =>
                            this.setState(state => {
                              return {
                                ...state,
                                valueById: {
                                  ...state.valueById,
                                  respon: text
                                }
                              };
                            })
                          }
                           value={this.state.valueById.respon}/>
                
                <Text>
                {"\n"}
                {"\n"}
                </Text>
                <View style={styles.bothButton} >
                    <TouchableHighlight  style={styles.submit} onPress={()=>{this.handleEditUser(this.state.valueById)}}>
                        <Text style={styles.submitText}>Edit</Text>
                    </TouchableHighlight>
                    <TouchableHighlight  style={styles.submit} underlayColor='#fff' onPress={()=>{this.handleDeleteUser(this.state.valueById.id)}}>
                        <Text style={styles.submitText}>Delete</Text>
                    </TouchableHighlight>
                    
                <Button color="grey" title="Delete" onPress={()=>this.props.navigation.navigate("Detail")}></Button>
                </View>
                </ScrollView>

                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    scroll:{
        width:350
    },
    img: {
        top: 0,
        left: 130,
        width: 80, 
        height: 80,
        borderRadius: 45,
    },
    name: {
        width: 300,
        backgroundColor: '#d3dbff',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        marginVertical: 10,
        top:40,
    },
    titleName:{
        top: 40,
    },
    picker:{
        top:40,
    },
    radio:{
        top: 43
    },
    bothButton:{
        flexDirection: 'row',
        justifyContent:"space-between",
        bottom: 10,
        margin: 25,
    },
    submit:{
        marginRight:40,
        marginLeft:20,
    },
    submitText:{
        paddingTop:20,
        paddingBottom:20,
        width:90,
        color:'#fff',
        elevation: 5,
        textAlign:'center',
        backgroundColor:'#68a0cf',
        borderRadius: 10,
        borderColor: '#fff'
    },
    camera:{
      top:20
    }

})

export default UserDetailContainer