import React,{ Component } from "react";
import {Picker} from '@react-native-community/picker';
import { View,Text,Image,StyleSheet, Button } from "react-native";
import { getUsersById, fetchAddNew } from "../user/UserService";
import { TextInput, ScrollView, TouchableOpacity, TouchableHighlight } from "react-native-gesture-handler";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import {DatePicker} from 'native-base'

class AddUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
                    nik:"",
                    name:"",
                    address:"",
                    birth_date:"",
                    telp:"",
                    status_kepegawaian:[
                        {label: 'Aktif Bekerja    ', value: 0 },
                        {label: 'Pensiunan', value: 1 }
                      ],
                    gaji:"",
                    bank:"",
                    respon:"",
                    
        }
        
    }
    
    handleSubmitNewUser=async()=>{
        await fetchAddNew(this.state)
        alert("Success mendaftar !")
        console.log(this.state)
        this.setState({...this.state})
    }
    render() {
     
        return (
            <View style={styles.container}>
            <ScrollView style={styles.scroll}>
                <TouchableHighlight style={styles.camera} onPress={()=>this.props.navigation.navigate("Camera")}>
                <Image source={{uri: 'https://reactnative.dev/img/tiny_logo.png'}}
            style={styles.img}
            />
                </TouchableHighlight>
            
            <Text style={styles.titleName}>NIK</Text>
            <TextInput style={styles.name}
                       underlineColorAndroid='rgba(0,0,0,0)'
                       keyboardType="phone-pad"
                       onChangeText={nik => this.setState({...this.state, nik})}
                       value={this.state.nik}
            />
            <Text style={styles.titleName}>Nama</Text>
            <TextInput style={styles.name}
                       underlineColorAndroid='rgba(0,0,0,0)'
                       onChangeText={name => this.setState({...this.state, name})}
                       value={this.state.name}
            />
            <Text style={styles.titleName}>Tanggal Lahir</Text>

            <Text>
                {"\n"}
                {"\n"}
                </Text>
                
                <DatePicker
                    defaultDate={new Date(2018, 4, 4)}
                    minimumDate={new Date(1915, 1, 1)}
                    maximumDate={new Date(2018, 12, 31)}
                    locale={"en"}
                    modalTransparent={false}
                    placeHolderText="Select date"
                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                    onDateChange={birth_date=> this.setState({...this.state, birth_date})}
                    value={this.state.birth_date}
                    disabled={false}
                 />
            
            <Text style={styles.titleName}>Alamat</Text>
            <TextInput style={styles.name}
                       underlineColorAndroid='rgba(0,0,0,0)'
                       onChangeText={address => this.setState({...this.state, address})}
                       value={this.state.address}
            />
            <Text style={styles.titleName}>No Telp</Text>
            <TextInput style={styles.name}
                       underlineColorAndroid='rgba(0,0,0,0)'
                       keyboardType="phone-pad"
                       onChangeText={telp => this.setState({...this.state, telp})}
                       value={this.state.telp}
            />
            <Text style={styles.titleName}>Status Kepegawaian</Text>
            <Text>
            {"\n"}
            {"\n"}
            </Text>
            <RadioForm
                radio_props={this.state.status_kepegawaian}
                initial={-1}
                formHorizontal={true} 
                labelHorizontal={true}
                buttonColor={'#2196f3'}
                animation={true}
                onPress={(text) =>  this.setState({...this.state, status_kepegawaian: {...this.state.status_kepegawaian, value: text}})}
                />
        
             <Text style={styles.titleName}>Gaji Aktif/Pensiun</Text>
            <TextInput style={styles.name}
                       underlineColorAndroid='rgba(0,0,0,0)'
                       keyboardType="phone-pad"
                       onChangeText={gaji => this.setState({...this.state, gaji})}
                       value={this.state.gaji}
            />

            <Text style={styles.titleName}>Pembayaran Gaji Dengan Bank</Text>
            <Picker
             selectedValue={this.state.bank}
             style={styles.picker}
             onValueChange={(bank, itemIndex) => this.setState({...this.state, bank})}
            >
                <Picker.Item label="--Pilih Bank--"/>
                <Picker.Item label="BCA" value="BCA"/>
                <Picker.Item label="BNI" value="BNI"/>
                <Picker.Item label="Mandiri" value="Mandiri"/>


            </Picker>

            <Text style={styles.titleName}>Respon</Text>
            <TextInput style={styles.name}
                        label="Respon"
                       underlineColorAndroid='rgba(0,0,0,0)'
                       onChangeText={respon => this.setState({...this.state, respon})}
                       value={this.state.respon}/>
            
            <Text>
            {"\n"}
            {"\n"}
            </Text>
            <View style={styles.bothButton} >
                <TouchableHighlight style={styles.submit} onPress={()=>this.handleSubmitNewUser()}>
                    <Text style={styles.submitText}>Submit</Text>
                </TouchableHighlight>
                
            </View>
            </ScrollView>

            
        </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    scroll:{
        width:350
    },
    img: {
        top: 0,
        left: 130,
        width: 80, 
        height: 80,
        borderRadius: 45,
    },
    name: {
        width: 300,
        backgroundColor: '#d3dbff',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        marginVertical: 10,
        top:40,
    },
    titleName:{
        top: 40,
    },
    picker:{
        top:40,
    },
    radio:{
        top: 43
    },
    bothButton:{
        flexDirection: 'row',
        justifyContent:"space-between",
        bottom: 10,
        margin: 25,
    },
    submit:{
        marginRight:40,
        marginLeft:20,
    },
    submitText:{
        paddingTop:20,
        paddingBottom:20,
        width:90,
        color:'#fff',
        elevation: 5,
        textAlign:'center',
        backgroundColor:'#68a0cf',
        borderRadius: 10,
        borderColor: '#fff'
    },
    camera:{
        top:20
    }

})
export default AddUser