import { LINK_URL_API } from "./constants/Constanta";
import { Alert } from "react-native";

export async function getUsers() {
    const data = await fetch(LINK_URL_API+"/users", {method: "GET"})
        .then((response)=> {
            return response.json();
        })
        return data;
}

export async function getUsersById(id) {
    const data = await fetch(LINK_URL_API+"/users/"+id, {method: "GET"})
        .then((response)=> {
            return response.json();
        })
        return data;
}

export async function fetchAddNew(user) {
    const data = await fetch(LINK_URL_API+"/users", {method:"POST",
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify(user)})
        .then((response) => {

            return response.json();
        }).catch(reason => {
            console.log(reason)
        });
    return data;
}
export async function updateDataUser(event) {
    const data = await fetch(LINK_URL_API+"/users/"+event.id, {method:"PUT",
        headers:{'Content-Type': 'application/json'},
        body: JSON.stringify(event)})
        return data;
}

export async function deleteDataUserById(id) {

    Alert.alert(
        'Perhatian !',
        'Apakah anda yakin ingin menghapus data ini ?',
        
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel'
          },
          { text: 'OK', onPress: () =>     
            fetch(LINK_URL_API+`/users/${id}`, {
            method: "DELETE", headers: {'Content-Type': 'application/json'},
        }).then((result) => {
            if(result.status===200) {
                Alert.alert('Konfirmasi', 'Data Berhasil Dihapus !')
            } 
            console.log(result)
    
            return result
        }) }
        ],
        { cancelable: false }
    );



}