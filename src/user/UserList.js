import React,{ Component } from "react";
import { View,Text, Alert,Button, Image, SafeAreaView, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import { getUsers } from "./UserService";
import {connect} from "react-redux"
import { fetchUsersSuccess } from "./UserAction";

import { createStackNavigator } from '@react-navigation/stack';
import { ScrollView } from "react-native-gesture-handler";

class UserList extends React.Component {

componentDidMount() {
  this.handleFetchData();
}
componentDidUpdate(){
  this.setState(this.state);
}

  
  handleFetchData = async () =>{
    let data = await getUsers();
    if(!(data===undefined)) {
        this.props.dispatch({...fetchUsersSuccess, payload: data})
    }
  }
  SampleFunction=()=>{
    this.props.navigation.navigate("AddNew")
  }
  
    render() {
      return(
          <View style={styles.container}>
            <ScrollView> 
              {this.props.users.map((value, key)=>{
                  return <TouchableOpacity style={styles.button}onPress={()=>this.props.navigation.navigate("Profile", {passingValue: value.id, passingAll: this.props, refresh: this.handleFetchData})}>
                  <Image style={styles.img} source={{uri: 'https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg'}} />
                  <Text style={styles.textName}>{value.name}</Text>
                  <Text style={styles.textTelp}>{value.telp}</Text>
                  <Text style={styles.textRespon}>{value.respon}</Text>
                  </TouchableOpacity>
              })}
            </ScrollView>
          <TouchableOpacity onPress={this.SampleFunction} style={styles.TouchableOpacityStyle} >
 
          <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2017/11/Floating_Button.png'}} 
          
                 style={styles.FloatingButtonStyle} />
       
        </TouchableOpacity>
          </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  button: {
    backgroundColor: "#d3dbff",
    margin:8,
    width:380,
    elevation: 5,
    position: 'relative',
    paddingVertical: 10,
    paddingHorizontal: 12,
    height: 150

  },
  text: {
    fontSize: 18,
    fontWeight: "bold",
    alignSelf: "center",
  },
  textName: {
    position: "absolute",
    width: 280,
    height: 27,
    left: 98,
    top: 25,
    fontSize: 18,
    fontWeight:"bold",
    lineHeight: 22,
  },
  textTelp: {
    position: "absolute",
    left: 98,
    top: 55,
    fontSize: 15,
    lineHeight: 22,
  },
  textRespon: {
    position: "absolute",
    width: 280,
    height: 47,
    left: 98,
    top: 87,
    fontSize: 15,
    lineHeight: 22,
  },
  btnNew:{
    margin: 10,
    padding: 10,
  },
  img: {
    width:75,
    height:75,
    marginTop:30,
    borderRadius:35
  },
  TouchableOpacityStyle:{
 
    position: 'absolute',
    width: 70,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
 
  FloatingButtonStyle: {
 
    resizeMode: 'contain',
    width: 70,
    height: 70,
  }

});

const mapStateToProps = (state) => {
  return {...state}
}
export default connect(mapStateToProps)(UserList)