import { createStore } from "redux";
import { Provider } from 'react-redux';
import userReducer from "./UserAction";
import React, { Component } from 'react';
import UserList from "./UserList";


class UserListContainer extends React.Component {
    render() {
      return (
        <Provider store={createStore(userReducer)}>
          <UserList {...this.props}/>
        </Provider>
      );
    };
}

export default UserListContainer