
export const fetchUsersSuccess = {type: 'FETCH_USERS_SUCCESS'};
export const fetchUsersByIdSuccess = {type: 'FETCHING_USERS_BY_ID_SUCCESS'};
export const handleUpdateUserNik = {type: 'HANDLE_UPDATE_USER_NIK'};
export const handleUpdateUserName = {type: 'HANDLE_UPDATE_USER_NAMA'};
export const handleUpdateUserAddress = {type: 'HANDLE_UPDATE_USER_ADDRESS'};
export const handleUpdateUserDate = {type: 'HANDLE_UPDATE_USER_BIRTH_DATE'};
export const handleUpdateUserTelp = {type: 'HANDLE_UPDATE_USER_TELP'};
export const handleUpdateUserStatus = {type: 'HANDLE_UPDATE_USER_STATUS'};
export const handleUpdateUserGaji = {type: 'HANDLE_UPDATE_USER_GAJI'};
export const handleUpdateUserBank = {type: 'HANDLE_UPDATE_USER_BANK'};
export const handleUpdateUserRespon = {type: 'HANDLE_UPDATE_USER_RESPON'};




const initialState = {
    users: [],
    loadingEvents: true,
    usersId:[],
    updateUser: {
        nik:"",
        name:"",
        address:"",
        birth_date:"",
        telp:"",
        status_kepegawaian:"",
        gaji:"",
        bank:"",
        respon:""
    }
}

export default function userReducer(state = initialState, action) {

    switch (action.type) {
        case "FETCH_USERS_SUCCESS":
            return {...state, users: action.payload, loadingEvents: false}
        case "FETCHING_USERS_BY_ID_SUCCESS":
            return {...state, usersId: action.payload}
        case "HANDLE_UPDATE_USER_NIK":
            return {...state, updateUser: {...state.updateUser, nik: action.nik}}
        case "HANDLE_UPDATE_USER_NAMA":
            return {...state, updateUser: {...state.updateUser, name: action.name}}
        case "HANDLE_UPDATE_USER_ADDRESS":
            return {...state, updateUser: {...state.updateUser, address: action.address}}
        case "HANDLE_UPDATE_USER_BIRTH_DATE":
            return {...state, updateUser: {...state.updateUser, birth_date: action.birth_date}}
        case "HANDLE_UPDATE_USER_TELP":
            return {...state, updateUser: {...state.updateUser, telp: action.telp}}
        case "HANDLE_UPDATE_USER_STATUS":
            return {...state, updateUser: {...state.updateUser, status_kepegawaian: action.status_kepegawaian}}
        case "HANDLE_UPDATE_USER_GAJI":
            return {...state, updateUser: {...state.updateUser, gaji: action.gaji}}
        case "HANDLE_UPDATE_USER_BANK":
            return {...state, updateUser: {...state.updateUser, bank: action.bank}}
        case "HANDLE_UPDATE_USER_RESPON":
            return {...state, updateUser: {...state.updateUser, respon: action.respon}}
        default: return state;
    }
}